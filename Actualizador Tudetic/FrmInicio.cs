﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Actualizador_Tudetic
{
    public partial class FrmInicio : Form
    {
        public enum ProgramaActualizar { Tudetic, Buscador }
        ProgramaActualizar QuePrograma { get; set; }
        int MilisegundosEspera = 3000;
        string Origen { get; set; }
        string Destino { get; set; }
        enum TipoActualizar { Normal, Servicios, Intranet }
        int MinutosEsperando = 0;
        int _ContadorDeFicheros;
        int ContadorDeFicheros
        {
            get
            {
                return _ContadorDeFicheros;
            }
            set
            {
                if (lblHaActualizado.Text == "NO")
                {
                    lblHaActualizado.Text = "SI";
                    panelActulizado.BackColor = Color.GreenYellow;
                }
            }
        }

        int TotalFicheros = 0;

        public FrmInicio(ProgramaActualizar quePrograma)
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
            QuePrograma = quePrograma;

            if(quePrograma == ProgramaActualizar.Tudetic)
            {
                btnCerrarProgramas.Text = "Cerrar Tudetic abiertos";
                labelTipo.Text = "Actualizador Tudetic ERP";
                //Origen = @"S:\Compilaciones\Tudetic";
                Origen = @"S:\FTP\COMPILACION TUDETIC";
                Destino = @"C:\Tudetic";
            }
            else if(quePrograma == ProgramaActualizar.Buscador)
            {
                btnCerrarProgramas.Text = "Cerrar Buscadores en ejecución";
                labelTipo.Text = "Actualizador Super Buscador";
                Origen = @"S:\Compilaciones\BuscadorAPI";
                Destino = @"C:\Buscador";
            }
        }

        private void FrmInicio_Load(object sender, EventArgs e)
        {
            lblFechaUltimaCompilacion.Text = DevolverFechaUltimaCompilacionTudetic().ToString("dd/MM/yyyy HH:mm:ss");
        }

        private void FrmInicio_Shown(object sender, EventArgs e)
        {
            List<TipoActualizar> actualizar = new List<TipoActualizar>() { TipoActualizar.Normal };

            Thread hilo = new Thread(delegate ()
            {
                bool arrancarAlAcabar = true;
                if (QuePrograma == ProgramaActualizar.Tudetic && Environment.MachineName == "SR-BURNS" || Environment.MachineName == "SERVIDOR" || Environment.MachineName == "USUARIOS" || Environment.MachineName == "SERVICIOS" || Environment.MachineName == "VIRTUALES" || Environment.MachineName == "SERVICIOS-DELL")
                {
                    if (MessageBox.Show("¿Quieres actualizar también SERVICIOS?", "¿Ambos?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        actualizar.Add(TipoActualizar.Servicios);
                    }
                    arrancarAlAcabar = false;
                }

                if (QuePrograma == ProgramaActualizar.Buscador)
                {
                    arrancarAlAcabar = false;
                }

                if (Environment.MachineName == "SR-BURNS")
                {
                    actualizar.Add(TipoActualizar.Intranet);
                }

                foreach(TipoActualizar tipo in actualizar)
                {
                    EscribirEnLog("Copiando RELEASE...");
                    bool correcto = false;

                    if (tipo == TipoActualizar.Normal)
                    {
                        while (correcto == false)
                        {
                            correcto = EjecutarCopia(arrancarAlAcabar);
                        }

                    }

                    if (tipo == TipoActualizar.Servicios)
                    {
                        Destino = @"C:\TudeticServicios";
                        correcto = false;
                        while (correcto == false)
                        {
                            EscribirEnLog("Copiando SERVICIOS...");
                            correcto = EjecutarCopia(arrancarAlAcabar);
                        }
                    }

                    if (tipo == TipoActualizar.Intranet)
                    {
                        Origen = @"C:\Users\Javier\source\repos\Intranet\WebIntranet";
                        Destino = @"C:\WebIntranet";
                        correcto = false;
                        while (correcto == false)
                        {
                            EscribirEnLog("Copiando INTRANET...");
                            correcto = EjecutarCopia(arrancarAlAcabar);
                        }
                    }
                }

                for (int i = 5; i > 0; i--)
                {
                    EscribirEnLog("Cerrando programa de actualizacion en " + i + " segundos...");
                    Thread.Sleep(1000);
                }

                Close();
            });

            hilo.Start();
        }

        private void EsperarAQueEstenTodosCerrados()
        {
            //Esto recorre los procesos y mira que el parametro enviado contenga el nombre

            bool abiertos = true;

            while(abiertos)
            {
                string[] comandos = Extensiones.GetAllCommandLines().OrderBy(o => o).ToArray();
                List<string> serviciosAbiertos = new List<string>();
                if (QuePrograma == ProgramaActualizar.Tudetic)
                {
                    serviciosAbiertos = comandos.Where(w => w.Contains("Actualizador") == false && w.Contains("Tudetic.exe") && w.Contains(Destino)).ToList();

                }
                else if(QuePrograma == ProgramaActualizar.Buscador)
                {
                    serviciosAbiertos = comandos.Where(w => w.ToLower().Contains("buscador") && w.ToLower().Contains(Destino.ToLower())).ToList();
                }

                if (serviciosAbiertos.Count == 0)
                {
                    EscribirEnLog($"No está Tudetic en uso, actualizamos");
                    break;
                }

                EscribirEnLog($"Hay {serviciosAbiertos.Count} servicios en ejecución:");
                foreach(string servicio in serviciosAbiertos)
                {
                    EscribirEnLog(servicio);
                }

                Thread.Sleep(MilisegundosEspera);
                MinutosEsperando++;
                EscribirEnLog($"Hemos esperado {MinutosEsperando} veces durante {MilisegundosEspera/1000}s a que se cierren los servicios...");
                if(MinutosEsperando >= 100)
                {
                    //Le pongo que cierre los programas que no le está haciendo ni caso nadie
                    CerrarProgramasAbiertos();
                }
            }        
        }

        private bool EjecutarCopia(bool arrancarProgramaAlAcabar)
        {
            try
            {
                CerrarProgramasAbiertos();          
                EsperarAQueEstenTodosCerrados();
                EscribirEnLog($"Actualizando programa de {Origen} a ... {Destino}");
                Thread.Sleep(1000);
                CopiarCarpeta(Origen);
                if (arrancarProgramaAlAcabar)
                {
                    if(QuePrograma == ProgramaActualizar.Tudetic)
                    {
                        ExecuteCommand(@"start c:\Tudetic\Tudetic.exe");
                    }
                    else if(QuePrograma == ProgramaActualizar.Buscador)
                    {
                        ExecuteCommand(@"start c:\Buscador\BuscadorAPI.exe");
                    }
                }
                EscribirEnLog("Copiados " + ContadorDeFicheros + " ficheros actualizados");
                return true;
            }
            catch (Exception ex)
            {
                EscribirEnLog($"No se ha podido copiar. ¿Tendrás que cerrar los {QuePrograma} que haya abiertos?");
            }
            return false;
        }

        private void EscribirEnLog(string Texto)
        {
            richTextBox1.Text = DateTime.Now.ToString("HH:mm:ss") + " -> " + Texto + "\n" + richTextBox1.Text;
            Application.DoEvents();
        }

        static void ExecuteCommand(string _Command)
        {
            //Indicamos que deseamos inicializar el proceso cmd.exe junto a un comando de arranque. 
            //(/C, le indicamos al proceso cmd que deseamos que cuando termine la tarea asignada se cierre el proceso).
            //Para mas informacion consulte la ayuda de la consola con cmd.exe /? 

            System.Diagnostics.ProcessStartInfo procStartInfo = new System.Diagnostics.ProcessStartInfo("cmd", "/c " + _Command);
            // Indicamos que la salida del proceso se redireccione en un Stream
            procStartInfo.RedirectStandardOutput = true;
            procStartInfo.UseShellExecute = false;
            //Indica que el proceso no despliegue una pantalla negra (El proceso se ejecuta en background)
            procStartInfo.CreateNoWindow = false;
            //Inicializa el proceso
            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.StartInfo = procStartInfo;
            proc.Start();
        }


        private void CopiarCarpeta(string carpeta)
        {
            string[] carpetas = Directory.GetDirectories(carpeta);
            foreach (string subcarpeta in carpetas)
            {
                CopiarCarpeta(subcarpeta);
            }

            string[] ficherosCompilacion = Directory.GetFiles(carpeta);

            foreach (string file in ficherosCompilacion)
            {
                TotalFicheros++;
                string original = Path.Combine(carpeta, file);
                string destino = original.Replace(Origen, Destino);

                if (!Directory.Exists(Path.GetDirectoryName(destino)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(destino));
                }

                if (!File.Exists(destino))
                {
                    CopiarFichero(original, destino);
                }
                else
                {
                    DateTime creacionOriginal = new FileInfo(original).LastWriteTime;
                    DateTime creacionDestino = new FileInfo(destino).LastWriteTime;
                    if (creacionOriginal > creacionDestino)
                    {
                        CopiarFichero(original, destino);
                    }
                }

                if (TotalFicheros % 1000 == 0) EscribirEnLog("Repasados " + TotalFicheros.ToString() + " ficheros");

            }
        }

        private void CopiarFichero(string original, string destino)
        {
            ContadorDeFicheros++;
            EscribirEnLog("Copiando fichero " + original);
            if(File.Exists(destino))
            {
                File.Delete(destino);
            }

            File.Copy(original, destino);
            
        }

        private void CerrarProgramasAbiertos()
        {
            string comando = "";
            if (QuePrograma == ProgramaActualizar.Tudetic)
            {
                foreach (var proceso in Process.GetProcessesByName("Tudetic"))
                {
                    try
                    {
                        string path = proceso.MainModule.FileName;
                        // Comprueba si es una versión en depuración o en release
                        if (!path.Contains("Debug"))
                        {
                            proceso.Kill();
                        }
                    }
                    catch (Exception ex1)
                    {
                        try
                        {
                            comando = "taskkill /F /IM Tudetic.exe";
                            Thread.Sleep(1000);
                        }
                        catch (Exception ex2)
                        {
                            EscribirEnLog("Error al cerrar proceso: " + ex1.Message + "\nY como segundo error ha dado este: " + ex2.Message);
                        }
                    }
                }
            }
            else if(QuePrograma == ProgramaActualizar.Buscador)
            {
                comando = "taskkill /F /IM  BuscadorAPI.exe";
            }
            ExecuteCommand(comando);
            EscribirEnLog(comando);
        }

        private void btnCerrarTodosTudetics_Click(object sender, EventArgs e)
        {
            CerrarProgramasAbiertos();
        }

        private void btnCerrarActualizador_Click(object sender, EventArgs e)
        {
            Close();
        }

        public DateTime DevolverFechaUltimaCompilacionTudetic()
        {
            DirectoryInfo di = new DirectoryInfo(Origen);
            return di.GetFiles().OrderByDescending(o => o.LastWriteTime).First().LastWriteTime;
        }


    }
}
