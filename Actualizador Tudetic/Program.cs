﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Actualizador_Tudetic
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if(args == null || args.Count() == 0 || args.First() == "TUDETIC")
            {
                Application.Run(new FrmInicio(FrmInicio.ProgramaActualizar.Tudetic));
            }
            else
            {
                Application.Run(new FrmInicio(FrmInicio.ProgramaActualizar.Buscador));
            }

        }
    }
}
