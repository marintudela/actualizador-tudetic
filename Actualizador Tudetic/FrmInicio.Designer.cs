﻿namespace Actualizador_Tudetic
{
    partial class FrmInicio
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmInicio));
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.btnCerrarProgramas = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelTipo = new System.Windows.Forms.Label();
            this.btnCerrarActualizador = new System.Windows.Forms.Button();
            this.panelActulizado = new System.Windows.Forms.Panel();
            this.lblHaActualizado = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblFechaUltimaCompilacion = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelActulizado.SuspendLayout();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.BackColor = System.Drawing.Color.Black;
            this.richTextBox1.ForeColor = System.Drawing.Color.LemonChiffon;
            this.richTextBox1.Location = new System.Drawing.Point(32, 92);
            this.richTextBox1.Margin = new System.Windows.Forms.Padding(2);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(644, 646);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // btnCerrarProgramas
            // 
            this.btnCerrarProgramas.BackColor = System.Drawing.Color.Black;
            this.btnCerrarProgramas.FlatAppearance.BorderSize = 0;
            this.btnCerrarProgramas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrarProgramas.ForeColor = System.Drawing.Color.White;
            this.btnCerrarProgramas.Location = new System.Drawing.Point(380, 60);
            this.btnCerrarProgramas.Name = "btnCerrarProgramas";
            this.btnCerrarProgramas.Size = new System.Drawing.Size(186, 27);
            this.btnCerrarProgramas.TabIndex = 3;
            this.btnCerrarProgramas.Text = "Cerrar";
            this.btnCerrarProgramas.UseVisualStyleBackColor = false;
            this.btnCerrarProgramas.Click += new System.EventHandler(this.btnCerrarTodosTudetics_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::Actualizador_Tudetic.Properties.Resources.Tudetic_esquina;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Location = new System.Drawing.Point(0, -24);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(580, 224);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // labelTipo
            // 
            this.labelTipo.AutoSize = true;
            this.labelTipo.Font = new System.Drawing.Font("Roboto Cn", 15.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.labelTipo.ForeColor = System.Drawing.Color.White;
            this.labelTipo.Location = new System.Drawing.Point(378, 32);
            this.labelTipo.Name = "labelTipo";
            this.labelTipo.Size = new System.Drawing.Size(121, 27);
            this.labelTipo.TabIndex = 4;
            this.labelTipo.Text = "Actualizador";
            // 
            // btnCerrarActualizador
            // 
            this.btnCerrarActualizador.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnCerrarActualizador.FlatAppearance.BorderSize = 0;
            this.btnCerrarActualizador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrarActualizador.Image = ((System.Drawing.Image)(resources.GetObject("btnCerrarActualizador.Image")));
            this.btnCerrarActualizador.Location = new System.Drawing.Point(678, 4);
            this.btnCerrarActualizador.Margin = new System.Windows.Forms.Padding(2);
            this.btnCerrarActualizador.Name = "btnCerrarActualizador";
            this.btnCerrarActualizador.Size = new System.Drawing.Size(26, 25);
            this.btnCerrarActualizador.TabIndex = 156;
            this.btnCerrarActualizador.UseVisualStyleBackColor = true;
            this.btnCerrarActualizador.Click += new System.EventHandler(this.btnCerrarActualizador_Click);
            // 
            // panelActulizado
            // 
            this.panelActulizado.BackColor = System.Drawing.Color.IndianRed;
            this.panelActulizado.Controls.Add(this.lblHaActualizado);
            this.panelActulizado.Location = new System.Drawing.Point(616, 752);
            this.panelActulizado.Name = "panelActulizado";
            this.panelActulizado.Size = new System.Drawing.Size(60, 56);
            this.panelActulizado.TabIndex = 157;
            // 
            // lblHaActualizado
            // 
            this.lblHaActualizado.AutoSize = true;
            this.lblHaActualizado.Font = new System.Drawing.Font("Roboto Cn", 15.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lblHaActualizado.ForeColor = System.Drawing.Color.White;
            this.lblHaActualizado.Location = new System.Drawing.Point(12, 16);
            this.lblHaActualizado.Name = "lblHaActualizado";
            this.lblHaActualizado.Size = new System.Drawing.Size(38, 27);
            this.lblHaActualizado.TabIndex = 159;
            this.lblHaActualizado.Text = "NO";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Roboto Cn", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(484, 768);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 24);
            this.label1.TabIndex = 158;
            this.label1.Text = "Ha actualizado:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Roboto Cn", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(36, 768);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(199, 24);
            this.label2.TabIndex = 159;
            this.label2.Text = "Fecha última compilación:";
            // 
            // lblFechaUltimaCompilacion
            // 
            this.lblFechaUltimaCompilacion.AutoSize = true;
            this.lblFechaUltimaCompilacion.Font = new System.Drawing.Font("Roboto Cn", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaUltimaCompilacion.ForeColor = System.Drawing.Color.White;
            this.lblFechaUltimaCompilacion.Location = new System.Drawing.Point(240, 768);
            this.lblFechaUltimaCompilacion.Name = "lblFechaUltimaCompilacion";
            this.lblFechaUltimaCompilacion.Size = new System.Drawing.Size(165, 24);
            this.lblFechaUltimaCompilacion.TabIndex = 160;
            this.lblFechaUltimaCompilacion.Text = "dd/MM/yyyy HH:mm ";
            // 
            // FrmInicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(709, 821);
            this.Controls.Add(this.lblFechaUltimaCompilacion);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panelActulizado);
            this.Controls.Add(this.btnCerrarActualizador);
            this.Controls.Add(this.labelTipo);
            this.Controls.Add(this.btnCerrarProgramas);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmInicio";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ACTUALIZADOR DE TUDETIC";
            this.Load += new System.EventHandler(this.FrmInicio_Load);
            this.Shown += new System.EventHandler(this.FrmInicio_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelActulizado.ResumeLayout(false);
            this.panelActulizado.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnCerrarProgramas;
        private System.Windows.Forms.Label labelTipo;
        private System.Windows.Forms.Button btnCerrarActualizador;
        private System.Windows.Forms.Panel panelActulizado;
        private System.Windows.Forms.Label lblHaActualizado;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblFechaUltimaCompilacion;
    }
}

